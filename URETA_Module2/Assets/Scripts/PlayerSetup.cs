﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public GameObject fpsModel;
    public GameObject nonFpsModel;

    public GameObject playerUiPrefab;
    [SerializeField] private GameObject theUI;
    [SerializeField] private Transform killFeedTransform;
    public GameObject killTextPrefab;
    private GameObject killText;

    public GameObject nameDisplay;

    public PlayerMovementController playerMovementController;
    public Camera fpsCamera;

    private Animator animator;
    public Avatar fpsAvatar, nonFpsAvatar;

    private ShootingScript shooting;

    private int killedEnemies = 0;

    public GameObject GetPlayerUI()
    {
        return theUI;
    }

    //public Transform GetKillFeedTransform()
    //{
    //    return KillFeedTransform;
    //}

    // Start is called before the first frame update
    void Start()
    {
        playerMovementController = this.GetComponent<PlayerMovementController>();
        animator = this.GetComponent<Animator>();

        fpsModel.SetActive(photonView.IsMine);
        nonFpsModel.SetActive(!photonView.IsMine);
        animator.SetBool("isLocalPlayer", photonView.IsMine);
        //animator.avatar = photonView.IsMine ? fpsAvatar : nonFpsAvatar;

        nameDisplay.GetComponent<Text>().text = photonView.Owner.NickName;

        shooting = this.GetComponent<ShootingScript>();

        if (photonView.IsMine)
        {
            this.animator.avatar = fpsAvatar;
        }

        else
        {
            this.animator.avatar = nonFpsAvatar;
        }

        if (photonView.IsMine)
        {
            theUI = Instantiate(playerUiPrefab);
            playerMovementController.fixedTouchField = theUI.transform.Find("RotationTouchField").GetComponent<FixedTouchField>();
            playerMovementController.joystick = theUI.transform.Find("Fixed Joystick").GetComponent<Joystick>();
            fpsCamera.enabled = true;

            this.theUI.transform.Find("FireButton").GetComponent<Button>().onClick.AddListener(() => shooting.Fire());

            killFeedTransform = this.theUI.transform.Find("Kill Feed");
        }
        else
        {
            playerMovementController.enabled = false;
            GetComponent<RigidbodyFirstPersonController>().enabled = false;
            fpsCamera.enabled = false;
        }

        GameEvents.eventSystem.onPlayerDeath += APlayerDied;
    }

    public void APlayerDied(string killer, int actorNumber, string killed)
    {
        GetComponent<PhotonView>().RPC("UpdateKillFeed", RpcTarget.All, killer, actorNumber, killed);
    }

    [PunRPC]
    public void UpdateKillFeed(string killer, int actorNumber, string killed)
    {
       if (photonView.IsMine)
        {
            killText = Instantiate(killTextPrefab);
            killText.transform.SetParent(killFeedTransform);
            //killText.transform.localScale = Vector3.one;

            killText.transform.Find("KillerText").GetComponent<Text>().text = killer;
            killText.transform.Find("KilledText").GetComponent<Text>().text = "killed";
            killText.transform.Find("TargetText").GetComponent<Text>().text = killed;

            Debug.Log("Killfeed elements should show up");

            Destroy(killText, 2.5f); // destroy after 2.5 seconds

            Debug.Log("Test: " + killer + " == " + photonView.Owner.NickName);

            // STATS
            if (actorNumber == photonView.Owner.ActorNumber)
            {
                Debug.Log("Sender.nickname == info.photonView.Owner.NickName");
                KilledAnEnemy();
            }
        }
    }

    public void KilledAnEnemy()
    {
        Debug.Log("KilledEnemies+++");
        this.killedEnemies++;

        if (this.killedEnemies >= 1)
        {
            Debug.Log(PhotonNetwork.NickName + " has killed 10 or more enemies");
            GetComponent<PhotonView>().RPC("EndGame", RpcTarget.All, PhotonNetwork.NickName);
        }
    }

    [PunRPC]
    public void EndGame(string player)
    {
        //if (photonView.IsMine)
        //{
            this.theUI.transform.Find("PlayerWonText").GetComponent<Text>().text =
             player + " has won the game!";

            StartCoroutine(LeaveRoomAfterDelay());
        //}
    }

    IEnumerator LeaveRoomAfterDelay()
    {
        yield return new WaitForSeconds(4.0f);
        Debug.Log("A player has left the Room!");
        GetComponent<PhotonView>().RPC("LeaveRoom", RpcTarget.All);
    }

    [PunRPC]
    public void LeaveRoom()
    {
        //PhotonNetwork.LeaveRoom(); this errors
        SceneManager.LoadScene("LobbyScene");
    }

    public override void OnLeftRoom()
    {
        Debug.Log("Left room!");
        SceneManager.LoadScene("LobbyScene");
    }
}
