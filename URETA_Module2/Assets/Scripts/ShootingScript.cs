﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class ShootingScript : MonoBehaviourPunCallbacks
{
    public Camera camera;
    public GameObject hitEffectPrefab;
    //public GameObject killTextPrefab;

    // private Transform killFeedTransform = null; // to parent the kill text

    [Header("HP Related Stuff")]
    public float startHealth = 100;
    private float health;
    public Image healthbar;
    private bool isAlive = true;

    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        health = startHealth;
        healthbar.fillAmount = health / startHealth;
        animator = this.GetComponent<Animator>();
    }

    public void Fire()
    {
        RaycastHit hit;
        Ray ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f));

        if (Physics.Raycast(ray, out hit, 200))
        {
            Debug.Log(hit.collider.gameObject.name);
            photonView.RPC("CreateHitEffects", RpcTarget.All, hit.point);
        }

        if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
        {
            hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 25);
        }
    }

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info)
    {
        this.health -= damage;
        this.healthbar.fillAmount = health / startHealth;

        if (health <= 0 && isAlive == true) // check if isAlive first
        {
            Die(); 
            Debug.Log(info.Sender.NickName + " killed " + info.photonView.Owner.NickName);
            GameEvents.eventSystem.PlayerDied(info.Sender.NickName, info.Sender.ActorNumber, info.photonView.Owner.NickName);
            //transform.GetComponent<PlayerSetup>().APlayerDied(info.Sender.NickName, info.photonView.Owner.NickName);
        }
    }

    [PunRPC] 
    public void CreateHitEffects(Vector3 position)
    {
        GameObject hitEffectGameObject = Instantiate(hitEffectPrefab, position, Quaternion.identity);
        Destroy(hitEffectGameObject, 0.2f);
    }

    [PunRPC]
    private void PlayerKilled(string killer, string theKilled)
    {
        //if (photonView.IsMine)
        //{
        //    if (this.killFeedTransform == null)
        //    {
        //        Debug.Log("Trying to find killFeed gameObject");
        //        killFeedTransform = this.transform.GetComponent<PlayerSetup>().GetKillFeedTransform();
        //        //killFeedTransform = this.transform.GetComponent<PlayerSetup>().GetPlayerUI().transform;
        //    }

        //    GameObject killText = Instantiate(killTextPrefab);
        //    killText.transform.SetParent(killFeedTransform);
        //    //killText.transform.localScale = Vector3.one;

        //    killText.transform.Find("KillerText").GetComponent<Text>().text = killer;
        //    killText.transform.Find("KilledText").GetComponent<Text>().text = "killed ";
        //    killText.transform.Find("TargetText").GetComponent<Text>().text = theKilled;

        //    Debug.Log("Killfeed elements should show up");

        //    Destroy(killText, 2.5f); // destroy after 2.5 seconds
        //}

        //Debug.Log("Test: " + killer + " == " + photonView.Owner.NickName);
        //// STATS
        //if (killer == photonView.Owner.NickName)
        //{
        //    Debug.Log("Sender.nickname == info.photonView.Owner.NickName");
        //    this.gameObject.GetComponent<PlayerSetup>().KilledAnEnemy(); // PROBLEM
        //}
    }

    public void Die()//(string killer, string theKilled)
    {
        //photonView.RPC("PlayerKilled", RpcTarget.All, killer, theKilled);
        //InstantiateKillText(killer, theKilled);

        if (photonView.IsMine)
        {
            this.isAlive = false;
            this.animator.SetBool("IsDead", true);
            //RespawnCountdown();
            StartCoroutine(RespawnCountdown());
        }
    }

    IEnumerator RespawnCountdown()
    {
        GameObject respawnText = GameObject.Find("RespawnText");
        float respawnTime = 5.0f;

        while (respawnTime > 0)
        {
            yield return new WaitForSeconds(1.0f);
            respawnTime--;

            transform.GetComponent<PlayerMovementController>().enabled = false;
            respawnText.GetComponent<Text>().text = "You are killed. Respawning in " + respawnTime.ToString(".00");
        }

        animator.SetBool("isDead", false);
        respawnText.GetComponent<Text>().text = "";

        SpawnManager.instance.Respawn(this.gameObject);

        transform.GetComponent<PlayerMovementController>().enabled = true;

        photonView.RPC("RegainHealth", RpcTarget.AllBuffered);
    }

    [PunRPC]
    public void RegainHealth()
    {
        Debug.Log("Regained health");

        health = 100;
        healthbar.fillAmount = health / startHealth;
    }
}
