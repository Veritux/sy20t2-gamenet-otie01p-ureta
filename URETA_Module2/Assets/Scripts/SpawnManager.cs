﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class SpawnManager : MonoBehaviourPunCallbacks
{
    public static SpawnManager instance;

    public GameObject playerPrefab;

    [SerializeField] private List<GameObject> spawnPoints = new List<GameObject>();

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }

    public GameObject SpawnUnit()
    {
       GameObject spawnedUnit = PhotonNetwork.Instantiate(playerPrefab.name, DetermineSpawnPoint(), Quaternion.identity);
        return spawnedUnit;
    }

    public void Respawn(GameObject unit)
    {
        unit.transform.position = DetermineSpawnPoint();
    }

    private Vector3 DetermineSpawnPoint()
    {
        int randomNumber = Random.Range(0, spawnPoints.Count);
        GameObject chosenSpawnZone = spawnPoints[randomNumber];

        Vector3 bounds = chosenSpawnZone.GetComponent<BoxCollider>().bounds.size;
        bounds.x /= 2;
        bounds.z /= 2;

        Vector3 spawnLocation;
        spawnLocation.x = chosenSpawnZone.transform.position.x + Random.Range(-bounds.x, bounds.x);
        spawnLocation.y = 0;
        spawnLocation.z = chosenSpawnZone.transform.position.z + Random.Range(-bounds.z, bounds.z);

        return spawnLocation;
    }
}
