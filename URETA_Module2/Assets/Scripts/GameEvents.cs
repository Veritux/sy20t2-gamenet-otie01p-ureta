﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEvents : MonoBehaviour
{
    public static GameEvents eventSystem;

    private void Awake()
    {
        eventSystem = this;
    }

    // Enemy Killed
    public event Action<string, int, string> onPlayerDeath;
    public void PlayerDied(string killer,int actorNumber, string killed) { onPlayerDeath?.Invoke(killer, actorNumber, killed); }
}
